package sondow.meadow;

import com.google.gson.Gson;
import com.sys1yagi.mastodon4j.MastodonClient;
import com.sys1yagi.mastodon4j.api.entity.Status;
import com.sys1yagi.mastodon4j.api.exception.Mastodon4jRequestException;
import com.sys1yagi.mastodon4j.api.method.Statuses;
import okhttp3.OkHttpClient;

public class Tooter {

    private MastodonConfig mastodonConfig;

    public Tooter(MastodonConfig mastodonConfig) {
        this.mastodonConfig = mastodonConfig;
    }

    public Status toot(Post post) {
        String mastodonInstanceName = mastodonConfig.getMastodonInstanceName();
        String mastodonAccessToken = mastodonConfig.getMastodonAccessToken();
        MastodonClient client = new MastodonClient.Builder(mastodonInstanceName,
                new OkHttpClient.Builder(), new Gson()).accessToken(mastodonAccessToken).build();
        Statuses statuses = new Statuses(client);
        try {
            String message = post.getBodyText();
            String desc = post.getShortDescription();
            Status status = statuses.postStatus(message, null, null, false, desc).execute();
            String msg = "Successfully tooted message: " + message + " with status " + status;
            System.out.println(msg);
            return status;
        } catch (Mastodon4jRequestException e) {
            e.printStackTrace();
        }

        return null;
    }
}
