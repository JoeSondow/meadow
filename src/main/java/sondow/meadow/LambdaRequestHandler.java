package sondow.meadow;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

/**
 * The function that AWS Lambda will invoke.
 *
 * @author @JoeSondow
 */
public class LambdaRequestHandler implements RequestHandler<Object, Object> {

    /*
     * (non-Javadoc)
     *
     * @see com.amazonaws.services.lambda.runtime.RequestHandler#handleRequest(java.
     * lang.Object, com.amazonaws.services.lambda.runtime.Context)
     */
    @Override public Object handleRequest(Object input, Context context) {
        new Bot().go();
        return null;
    }

    public static void main(String[] args) {
        new LambdaRequestHandler().handleRequest(null, null);
    }
}
