package sondow.meadow;

public class BotConfigFactory {

    private Environment environment;

    public BotConfigFactory(Environment environment) {
        this.environment = environment;
    }

    public BotConfigFactory() {
        this(new Environment());
    }

    public BotConfig configure() {
        MastodonConfig mastodonConfig = configureMastodon();
        BlueskyConfig blueskyConfig = configureBluesky();

        return new BotConfig(blueskyConfig, mastodonConfig);
    }

    /**
     * @return configuration containing Mastodon authentication strings and other variables
     */
    private MastodonConfig configureMastodon() {

        MastodonConfig mastodonConfig = null;

        String credentialsCsv = environment.get("cred_mastodon");
        if (credentialsCsv != null) {
            String[] tokens = credentialsCsv.split(",");
            String instanceName = tokens[0];
            String screenName = tokens[1];
            String accessToken = tokens[2];
            mastodonConfig = new MastodonConfig(instanceName, accessToken);
        }
        return mastodonConfig;
    }

    /**
     * @return configuration containing Bluesky authentication strings and other variables
     */
    private BlueskyConfig configureBluesky() {
        String appPassword = environment.get("cred_bluesky");
        BlueskyConfig blueskyConfig = new BlueskyConfig("bsky.social", "emojimeadow",
                appPassword);
        return blueskyConfig;
    }
}
