package sondow.meadow;

import java.time.ZonedDateTime;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONObject;

public class Bluesky {

    private final BlueskyConfig blueskyConfig;

    public Bluesky(BlueskyConfig blueskyConfig) {
        this.blueskyConfig = blueskyConfig;
    }

    public void post(String message) {
        String server = blueskyConfig.getServerName();
        String screenName = blueskyConfig.getScreenName();
        String appPassword = blueskyConfig.getAppPassword();

        OkHttpClient.Builder okHttpClientbuilder = new OkHttpClient.Builder();
        OkHttpClient okHttpClient = okHttpClientbuilder.build();

        //        String handle = "emojimeadow.bsky.social";
        String handle = screenName.toLowerCase() + "." + server;
        String didUrl = "https://bsky.social/xrpc/com.atproto.identity.resolveHandle";

        HttpUrl.Builder httpBuilder = HttpUrl.parse(didUrl).newBuilder();
        httpBuilder.addQueryParameter("handle", handle);
        HttpUrl httpUrl = httpBuilder.build();
        Request getDidRequest = new Request.Builder().url(httpUrl).build();

        try {
            Response didResponse = okHttpClient.newCall(getDidRequest).execute();
            String didJson = didResponse.body().string();
            //            System.out.println(didJson);
            JSONObject didJsonObj = new JSONObject(didJson);
            String did = didJsonObj.getString("did");

            String apiKeyUrl = "https://bsky.social/xrpc/com.atproto.server.createSession";
            String postData =
                    "{\"identifier\": \"" + did + "\", \"password\": \"" + appPassword + "\"}";

            MediaType appJsonMediaType = MediaType.get("application/json");
            RequestBody apiPostBody = RequestBody.create(postData, appJsonMediaType);
            Request.Builder apiReqBuilder = new Request.Builder();
            apiReqBuilder.url(apiKeyUrl);
            Request apiRequest = apiReqBuilder.post(apiPostBody).build();
            Response apiResponse = okHttpClient.newCall(apiRequest).execute();
            String apiKeyJson = apiResponse.body().string();
            //            System.out.println(apiKeyJson);
            String apiKey = new JSONObject(apiKeyJson).getString("accessJwt");

            String postFeedUrl = "https://bsky.social/xrpc/com.atproto.repo.createRecord";
            Time time = new Time();
            ZonedDateTime now = time.now();
            String dateString = time.zFormat(now);
            String encodedMessage = message.replaceAll("\n", "\\\\n");
            String postRecordData = "{ \"collection\": \"app.bsky.feed.post\", " +
                    "\"repo\": \"" + did + "\", " +
                    "\"record\": { \"text\": \"" + encodedMessage + "\", " +
                    "\"createdAt\": \"" + dateString + "\", " +
                    "\"$type\": \"app.bsky.feed.post\" } }";
            //            System.out.println(postRecordData);

            RequestBody createPostBody = RequestBody.create(postRecordData, appJsonMediaType);
            Request.Builder postReqBuilder = new Request.Builder();
            postReqBuilder.url(postFeedUrl).post(createPostBody);
            postReqBuilder.header("Authorization", "Bearer " + apiKey);
            Request postFeedRequest = postReqBuilder.build();
            Response postFeedResponse = okHttpClient.newCall(postFeedRequest).execute();
            //            System.out.println(postFeedResponse.body().string());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        BotConfig botConfig = new BotConfigFactory().configure();
        BlueskyConfig blueskyConfig = botConfig.getBlueskyConfig();
        //        new Bluesky(blueskyConfig).post("Hello world " + new Random().nextInt());
    }
}
