package sondow.meadow;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

public class Time {

    ZonedDateTime now() {
        ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
        System.out.println("now UTC is " + now.format(DateTimeFormatter.ISO_DATE_TIME));
        return now;
    }

    private DateTimeFormatter NUMERICAL_FORMATTER = new DateTimeFormatterBuilder()
            .appendPattern("yyyy-MM-dd,HH:mm:ss.SSS").toFormatter().withZone(ZoneOffset.UTC);

    public String zFormat(ZonedDateTime date) {

        // "2023-04-29T20:11:34.733Z"
        return date.format(NUMERICAL_FORMATTER).replace(",", "T") + "Z";
    }
}
