package sondow.meadow;

public class Post {

    /**
     * Mastodon has a content warning feature that lets you hide the body of the post behind a
     * description of the contents. Since people using screen-readers could get annoyed hearing the
     * contents of a long post that's just a series of emojis, we use a short description of the
     * post as the content warning or subject of the post.
     */
    private String shortDescription;

    /**
     * The message of the post, containing all the emojis and whitespace that will be displayed.
     */
    private String bodyText;

    public Post(String shortDescription, String bodyText) {
        this.shortDescription = shortDescription;
        this.bodyText = bodyText;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getBodyText() {
        return bodyText;
    }
}
