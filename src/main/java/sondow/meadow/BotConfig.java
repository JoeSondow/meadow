package sondow.meadow;

public class BotConfig {

    private BlueskyConfig blueskyConfig;
    private MastodonConfig mastodonConfig;

    public BotConfig(BlueskyConfig blueskyConfig,
            MastodonConfig mastodonConfig) {
        this.blueskyConfig = blueskyConfig;
        this.mastodonConfig = mastodonConfig;
    }

    public BlueskyConfig getBlueskyConfig() {
        return blueskyConfig;
    }

    public MastodonConfig getMastodonConfig() {
        return mastodonConfig;
    }
}
