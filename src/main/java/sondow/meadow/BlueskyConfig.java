package sondow.meadow;

public class BlueskyConfig {

    private String serverName;
    private String screenName;
    private String appPassword;

    public BlueskyConfig(String serverName, String screenName, String appPassword) {
        this.serverName = serverName;
        this.screenName = screenName;
        this.appPassword = appPassword;
    }

    public String getServerName() {
        return serverName;
    }

    public String getScreenName() {
        return screenName;
    }

    public String getAppPassword() {
        return appPassword;
    }
}
