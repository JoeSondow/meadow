package sondow.meadow

import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import spock.lang.Specification

class GridSpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    def "create, get, put, and toString should all work right"() {
        setup:
        Grid grid = new Grid(5, 7, "M")

        when:
        String init = grid.toString()
        String row1col0Before = grid.getCellContents(1, 0)
        grid.put(1, 0, "P");
        grid.put(2, 3, "Q");
        String row1col0After = grid.getCellContents(1, 0)
        String row1col1After = grid.getCellContents(1, 1)
        String after = grid.toString()

        then:
        init == "MMMMMMM\nMMMMMMM\nMMMMMMM\nMMMMMMM\nMMMMMMM"
        row1col0Before == "M"
        row1col0After == "P"
        row1col1After == "M"
        after == "MMMMMMM\nPMMMMMM\nMMMQMMM\nMMMMMMM\nMMMMMMM"
    }

    def "edges cells should be easily determined"() {
        setup:
        Grid grid = new Grid(5, 7, "M")

        when:
        String init = grid.toString()
        grid.put(0, 0, "L")
        grid.put(0, 6, "R")
        grid.put(4, 0, "B")
        grid.put(4, 6, "G")
        String after = grid.toString()
        Cell topLeft = grid.getCell(0, 0);
        Cell topRight = grid.getCell(0, 6);
        Cell bottomLeft = grid.getCell(4, 0);
        Cell bottomRight = grid.getCell(4, 6);
        Cell topMiddle = grid.getCell(0, 3);
        Cell bottomMiddle = grid.getCell(4, 3);
        Cell midLeft = grid.getCell(2, 0);
        Cell midRight = grid.getCell(2, 6);
        Cell center = grid.getCell(3, 3);

        then:
        init == "MMMMMMM\nMMMMMMM\nMMMMMMM\nMMMMMMM\nMMMMMMM"

        after == "LMMMMMR\nMMMMMMM\nMMMMMMM\nMMMMMMM\nBMMMMMG"

        grid.isEdgeCell(topLeft)
        grid.isEdgeCell(topRight)
        grid.isEdgeCell(bottomLeft)
        grid.isEdgeCell(bottomRight)
        grid.isEdgeCell(topMiddle)
        grid.isEdgeCell(bottomMiddle)
        grid.isEdgeCell(midLeft)
        grid.isEdgeCell(midRight)
        !grid.isEdgeCell(center)
    }
}
