package sondow.meadow

import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import spock.lang.Specification

class BotConfigFactorySpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    //    def "configure should populate a configuration with environment variables"() {
    //        setup:
    //        FileClerk fileClerk = Mock()
    //        Environment environment = new Environment(new Keymaster(fileClerk))
    //        String filler = Environment.SPACE_FILLER
    //        envVars.set("cred_twitter",
    //                "${filler}disney,mickey,donald,goofy,pluto${filler}")
    //
    //        when:
    //        BotConfigFactory factory = new BotConfigFactory(environment)
    //
    //        then:
    //        with(config) {
    //            user == 'disney'
    //            OAuthConsumerKey == 'mickey'
    //            OAuthConsumerSecret == 'donald'
    //            OAuthAccessToken == 'goofy'
    //            OAuthAccessTokenSecret == 'pluto'
    //        }
    //        1 * fileClerk.readTextFile('build.properties') >> 'root.project.name=castle'
    //        1 * fileClerk.readTextFile('castle-values.json')
    //        1 * fileClerk.readTextFile('../cipher/castle-encryption-keys.json')
    //        1 * fileClerk.readTextFile('castle-encryption-keys.json')
    //        1 * fileClerk.readTextFile('../crypt/castle-values.json')
    //        0 * _._
    //    }
}
